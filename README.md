This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Higher Order Component

**Description** - A `HOC` is a function that takes a Component as an argument and returns a new component.

```jsx
import React, { useState } from 'react';

/**
 * @description Higher order custom component 
 */
const Custom = (OriginalComponent) => {

  const WithCustom = () => {
    
    const [state, setState] = useState({
      value: 0
    });

    const task = () => {
      setState({
        // update the value
      });
    };

    return <OriginalComponent value={state.value} task={task} />
  };

  return WithCustom;
}

export default Custom;
```

setup a Higher Order Component (HOC) with a normal component.

```jsx
import React, { Fragment } from 'react';
import Custom from './withCustom';

const Clicker = ({count, task}) => {

  return (
    <Fragment>
      <div>{count}</div>
      <button onClick={task}>+</button>
    </Fragment>
  );
}

export default Custom(Clicker);
```

usage of Higher Order Components in React.

```jsx
import { Custom } from './components';

...

/**
 * @description render function of return for functional components.
 */
return (
  <Custom />
);
```

import React, { useState } from 'react';

import { Clicker, HoverElement } from './components';

import './App.css';

const App = () => {

  const [state] = useState({
    elements: [
      {
        name: 'Hover 1',
        type: 'hover',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Clicker 1',
        type: 'clicker',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Clicker 2',
        type: 'clicker',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Clicker 3',
        type: 'clicker',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Hover 2',
        type: 'hover',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Hover 3',
        type: 'hover',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Clicker 4',
        type: 'clicker',
        className: 'm-l-16 m-r-16 m-b-10'
      },
      {
        name: 'Clicker 5',
        type: 'clicker',
        className: 'm-l-16 m-r-16 m-b-10'
      }
    ]
  });

  return (
    <div className="App flex-container row wrap flex-center align-center">
      {
        state.elements.map((el, key) => (
          el.type === 'hover'
          ? <HoverElement key={key} name={el.name} className={el.className} />
          : <Clicker key={key} name={el.name} className={el.className} />
        ))
      }
    </div>
  );
}

export default App;

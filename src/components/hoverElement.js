import React from 'react';
import Counter from './withCounter';

import './hoverElement.css';

const HoverElement = ({count, name, incrementCount, className}) => {
  
  return (
    <div className={`hover-element card ${className}`} onMouseEnter={incrementCount}>
      <div className={'m-b-10'}>Element: {name}</div>
      <div>Counter: {count}</div>
    </div>
  );
}

export default Counter(HoverElement);
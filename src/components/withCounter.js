import React, { useState } from 'react';

/**
 * @description Higher order counter component 
 */
const Counter = (OriginalComponent) => {

  const WithCounter = ({name, className}) => {
    
    const [state, setState] = useState({
      count: 0
    });
  
    const incrementCount = () => {
      setState({
        count: state.count + 1
      });
    };

    const decrementCount = () => {
      state.count > 0 && setState({
        count: state.count - 1
      });
    };
    
    return <OriginalComponent count={state.count} name={name} className={className} incrementCount={incrementCount} decrementCount={decrementCount} />
  };

  return WithCounter;
}

export default Counter;
import React from 'react';
import Counter from './withCounter';

import './clicker.css';

const Clicker = ({count, name, className, incrementCount, decrementCount}) => {

  return (
    <div className={`click-element card ${className}`}>
      <div className={'m-b-10'}>Element: {name}</div>
      <div className={'flex-container row flex-between'}>
        <button className={'button danger'} onClick={decrementCount}>-</button>
        <div>{count}</div>
        <button className={'button success'} onClick={incrementCount}>+</button>
      </div>
    </div>
  );
}

export default Counter(Clicker);